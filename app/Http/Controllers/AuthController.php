<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('register');
    }

    public function sending(Request $req){
        // dd($req->all());
        $namanya = $req["nama_awal"];
        $namanya2 = $req["nama_akhir"];
        return view('welcome', compact('namanya','namanya2'));

        // compact cara kedua
        // return view('welcome', ['namanya' => $namanya, 'namanya2' => $namanya2]); 
    }
}
