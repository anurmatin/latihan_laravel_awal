@extends('layout.master')

@section('judul')
    Halaman Selamat Datang
@endsection

@section('content')
    <h1>Selamat Datang {{ $namanya }} {{ $namanya2 }}, </h1><br>
    <h2>Terima kasih telah bergabung di Sanberbook. Social Media kita Bersama!</h2>
@endsection