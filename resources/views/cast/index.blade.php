@extends('layout.master')

@section('judul')
    List Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary mb-3">Tambah Cast</a>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      @forelse ($cast as $key => $item)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->umur }}</td>
            <td>{{ $item->bio }}</td>
            <td>
                <a href="/cast/{{ $item->id }}" class="btn btn-info btn-sm">Show</a>
                <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
            </td>
        </tr>
      @empty
        <tr>
            <td>Data masih kosong.</td>
        </tr>
      @endforelse
  </tbody>
</table>

@endsection